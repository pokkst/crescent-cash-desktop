# Crescent Cash Desktop

The desktop version of Crescent Cash.

The .exe for Windows is generated using Launch4J.

The .app for macOS is generated using jar2app.